import React, {Component} from 'react';
import { StyleSheet, Text, View, TextInput, Image, Button } from 'react-native';
import Hello from './Hello';


export default class App extends Component {

constructor(props) {
  super(props);
  this.state = {
    zip:"7.5",
    forecast: null
  };
}
_handleTextChange = event => {this.setState({ zip: event.nativeEvent.text }); //callback
};

  render() {
    
      let pic = {uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'};
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
        Your input {this.state.zip}
        </Text>
       <TextInput
        style={styles.input}
        onSubmitEditing={this._handleTextChange}/> 
         <Image source={pic} style={{width: 193, height: 110}}/>
         <Hello/>
         <Button
         title="Press me"
         accessibilityLabel="See information"
         />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions:{
    textAlign:'center',
    color: '#333333',
    marginBottom: 5,
  },
  input:{
  fontSize:20,
  borderWidth: 2,
  height: 40,
  width: 100,
  padding: 2,
  textAlign: 'center',
  },
});
